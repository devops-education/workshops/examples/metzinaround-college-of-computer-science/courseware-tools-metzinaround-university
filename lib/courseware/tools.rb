 #!/usr/bin/env ruby

require_relative "tools/version"
require "gitlab"
require "yaml"

DEVELOPER_ACCESS_LEVEL = 30
UNLOCKABLE = "unlockable"

@g = Gitlab.client(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV["API_TOKEN"],
)

@log = Logger.new(STDOUT)

def get_project(project_name, group_id)
  projects = @g.group_projects(group_id)
  for p in projects
    if p.name == project_name
      return p
    end
  end
  return nil
end

def get_group_id(group_name)
  # Seems like Gitlab API fails when using the name instead of the id sometimes
  # This workaround finds the id for the group we are using
  groups = @g.groups()
  group_id = -1
  for group in groups
    if group.name == group_name
      group_id = group.id
    end
  end
  @log.error("Group #{group_name} couldn't be found. Please check if it was spelled correctly.")
  return group_id
end

def assign_project(project, member, group_id, issues, course_bot)
  student_id = member.id
  student_username = member.username

  student_project_name = "#{project.name}-#{student_username}"
  student_project_path = "#{project.path}_#{student_username}"

  @log.info("Creating project: #{student_project_name} for student: #{student_username}")

  if not get_project(student_project_name, group_id).nil?
    @log.info("Project already exists, skipping.")
    return
  end

  # Create new project from "template"
  new_project = @g.create_fork(project.id)
  unforked_project = @g.remove_forked(new_project.id)

  @g.edit_project(new_project.id, {'name': student_project_name, 'path': student_project_path, 'visibility': "private"})
  @g.transfer_project(new_project.id, group_id)

  # Clone issues and assign their badges
  issue_num = 1
  skip = false
  for issue in issues
    for label in issue.labels
      if label == UNLOCKABLE
        skip = true
      end
    end
    if skip
      skip = false
      next
    end
    new_issue = @g.create_issue(new_project.id, issue.title, {'description': issue.description, 'due_date': issue.due_date})
    @g.add_project_badge(new_project.id, {link_url: new_issue.web_url, image_url: "https://img.shields.io/badge/issue#{issue_num}-not%20done-red"})
    issue_num = issue_num + 1
  end

  # Add Webhooks for issues
  if not course_bot.nil?
    @log.info("Adding project Webhook")
    @g.add_project_hook(new_project.id, "#{course_bot}/issue", {'issues_events': true, 'push_events': false})
  end

  # Add student to repository
  @g.add_team_member(new_project.id, student_id, DEVELOPER_ACCESS_LEVEL)  
end


course_data = YAML.load(File.read(ENV["COURSE_CONFIG"]))
for course_name in course_data.keys
  @log.info("Creating assignments for #{course_name}")
  course = course_data[course_name]
  project_name = course["template_name"]
  @log.info("Using project #{project_name} as the assignment template")
  group_name = course["group_name"]
  @log.info("Using group #{group_name} for assignment creation")
  educators = course["educators"]
  @log.info("#{educators} are the educators of this course, and will be added as admins to all repositories")
  if course.key?("course_bot")
    course_bot = course["course_bot"]
    @log.info("Using #{course_bot} as the endpoint for the Webhook")
  end

  group_id = get_group_id(group_name)
  project = self.get_project(project_name, group_id)

  # Obtain its issues
  issues =  @g.issues(project.id)

  # Obtain the list of students from the group
  group_members = @g.group_members(group_id)
  for member in group_members
    if not educators.include? member.username
      assign_project(project, member, group_id, issues, course_bot)
    end
  end
end